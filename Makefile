.PHONY: test

test:
	coverage run --source='.' manage.py test
	coverage report

run:
	python manage.py runserver
