from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from discounts.api_v1 import views


urlpatterns = [
    path("campaign", views.CampaignView.as_view({"post": "create"})),
    path("campaign/<campaign_pk>/code", views.CodeView.as_view({"get": "get"})),
]

urlpatterns = format_suffix_patterns(urlpatterns)
