from rest_framework import serializers

from discounts.models import (
    Campaign,
    Code,
)


class CampaignSerializer(serializers.Serializer):
    name = serializers.CharField()
    id = serializers.CharField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    # Since in current implementation we create codes inplace we want to limit max numbers
    # of generated codes.
    max_codes = serializers.IntegerField(min_value=1, max_value=100)

    def create(self, validated_data):
        user = self.context["request"].user
        return self.Meta.model.objects.create(
            owner=user,
            **validated_data,
        )

    class Meta:
        model = Campaign
        fields = ("id", "name", "max_codes", "created_at")


class CodeSerializer(serializers.Serializer):
    code = serializers.CharField(read_only=True)

    class Meta:
        model = Code
        fields = ("code",)
