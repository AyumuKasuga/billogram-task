from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST,
    HTTP_200_OK,
)
from rest_framework.exceptions import (
    NotFound,
    ValidationError,
)

from discounts.api_v1.serializers import (
    CampaignSerializer,
    CodeSerializer,
)
from discounts.models import (
    Code,
    Campaign,
)


class CampaignView(viewsets.ViewSet):
    """Campaign view allows a partner to create a campaign."""
    http_method_names = ["post"]

    def create(self, request, **kwargs):
        serializer = CampaignSerializer(
            data=request.data,
            context={"request": request},
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class CodeView(viewsets.ViewSet):
    """Code view allows a customer to get a discount code."""
    http_method_names = ["get"]

    def get(self, request, campaign_pk, **kwargs):
        campaign = Campaign.objects.filter(pk=campaign_pk).first()
        if not campaign:
            raise NotFound
        code = Code.objects.assign(campaign, request.user)
        if not code:
            raise ValidationError(
                detail="This campaign is ended and we have no more codes.",
                code="no_more_codes",
            )
        serializer = CodeSerializer(instance=code)
        return Response(serializer.data, status=HTTP_200_OK)
