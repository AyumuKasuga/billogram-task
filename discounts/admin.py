from django.contrib import admin

from .models import (
    Campaign,
    Code,
)


class CampaignAdmin(admin.ModelAdmin):
    list_display = ("id", "owner", "name", "max_codes", "created_at")


class CodeAdmin(admin.ModelAdmin):
    list_display = ("code", "campaign", "assigned_to")


admin.site.register(Campaign, CampaignAdmin)
admin.site.register(Code, CodeAdmin)
