from django.test import (
    TestCase,
    Client,
)

from django.contrib.auth.models import User

from discounts.models import (
    Campaign,
    Code,
)


class PartnerApiTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.path = "/v1/campaign"

    def test_unauthorized(self):
        resp = self.client.post(path=self.path)
        self.assertEqual(resp.status_code, 403)

    def test_get(self):
        resp = self.client.post(
            path=self.path,
            HTTP_AUTHUSER="partner",
        )
        self.assertEqual(resp.status_code, 400)

    def test_without_name(self):
        resp = self.client.post(
            path=self.path,
            content_type="application/json",
            data={"max_codes": 10},
            HTTP_AUTHUSER="partner",
        )
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(resp.json(), {"name": ["This field is required."]})

    def test_without_max_codes(self):
        resp = self.client.post(
            path=self.path,
            content_type="application/json",
            data={"name": "test"},
            HTTP_AUTHUSER="partner",
        )
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(resp.json(), {"max_codes": ["This field is required."]})

    def test_non_int_max_codes(self):
        resp = self.client.post(
            path=self.path,
            content_type="application/json",
            data={
                "max_codes": "wrong",
                "name": "test",
            },
            HTTP_AUTHUSER="partner",
        )
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(resp.json(), {"max_codes": ["A valid integer is required."]})

    def test_correct_params(self):
        resp = self.client.post(
            path=self.path,
            content_type="application/json",
            data={
                "max_codes": 7,
                "name": "test",
            },
            HTTP_AUTHUSER="partner",
        )
        self.assertEqual(resp.status_code, 201)
        campaign_id = resp.json()["id"]

        # Let's check database entities.
        campaign = Campaign.objects.get(pk=campaign_id)
        self.assertEqual(campaign.max_codes, 7)
        self.assertEqual(campaign.name, "test")
        self.assertEqual(
            Code.objects.filter(campaign=campaign, assigned_to=None).count(),
            7,
        )


class CustomerApiTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.path = "/v1/campaign/{}/code"
        user = User.objects.create()
        self.campaign = Campaign.objects.create(
            name="test",
            max_codes=3,
            owner=user,
        )

    def test_not_fount(self):
        resp = self.client.get(
            path=self.path.format("wrong_pk"),
            HTTP_AUTHUSER="customer",
        )
        self.assertEqual(resp.status_code, 404)

    def test_unauthorized(self):
        resp = self.client.get(path=self.path.format(self.campaign.pk))
        self.assertEqual(resp.status_code, 403)

    def test_get(self):
        resp = self.client.post(
            path=self.path.format(self.campaign.pk),
            HTTP_AUTHUSER="customer",
        )
        self.assertEqual(resp.status_code, 405)

    def test_no_more_codes(self):
        Code.objects.all().delete()
        resp = self.client.get(
            path=self.path.format(self.campaign.pk),
            HTTP_AUTHUSER="customer",
        )
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(
            resp.json(),
            [
                {
                    "message": "This campaign is ended and we have no more codes.",
                    "code": "no_more_codes",
                },
            ],
        )

    def test_assign_code(self):
        resp = self.client.get(
            path=self.path.format(self.campaign.pk),
            HTTP_AUTHUSER="customer",
        )
        self.assertEqual(resp.status_code, 200)
        code_id = resp.json()["code"]

        # Check that second time we receive the same code.
        resp = self.client.get(
            path=self.path.format(self.campaign.pk),
            HTTP_AUTHUSER="customer",
        )
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json()["code"], code_id)
