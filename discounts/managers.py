import logging
from django.contrib.auth.models import User
from django.db import models
from django.db.transaction import (
    atomic,
    on_commit,
)


logger = logging.getLogger(__name__)


class CodeManager(models.Manager):
    @atomic()
    def assign(self, campaign, user):
        """Assign available(unused) code to the given user and return the code.

        If a code for a given campaign and customer is already assigned --
        this function just returns it. In this implementation a customer can only
        have one discount code per campaign.

        :param campaign: Campaign instance.
        :param user: User instance.

        :return code: Code instance assigned to the given user.

        """
        # We need to have some lock to avoid situation when a user can
        # get multiple codes. In this case we just lock a user entry.
        User.objects.select_for_update().get(pk=user.pk)
        existent_code = self.model.objects.filter(
            campaign=campaign,
            assigned_to=user,
        ).first()
        # Just return the existent code.
        if existent_code:
            return existent_code
        # SKIP LOCKED allows us to not lock the table/row in case if multiple
        # users want to get their codes at the same time.
        available_code = (
            self.model.objects.select_for_update(
                skip_locked=True,
            )
            .filter(
                campaign=campaign,
                assigned_to=None,
            )
            .first()
        )
        if not available_code:
            return
        available_code.assigned_to = user
        available_code.save()
        # Here we can use async task or signal to notification service
        # to notify a customer and campaign owner about this event.
        on_commit(
            lambda: logger.info(
                "User {} has received a coupon code from campaign {}".format(
                    user.pk,
                    campaign.name,
                )
            )
        )
        return available_code
