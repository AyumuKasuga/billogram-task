FROM python:3.8

EXPOSE 8000/tcp

WORKDIR /loyalty_program

RUN pip install pip -U && pip install pipenv

COPY Pipfile* ./

RUN pipenv install --deploy --system

HEALTHCHECK --interval=30s --timeout=3s CMD curl -f http://localhost:8000/healthcheck/ || exit 1

# This docker file is not production ready and only here for tests and development
# So we don't copy source files inside the container
# COPY * .

ENTRYPOINT ["python", "manage.py"]
