from rest_framework.authentication import RemoteUserAuthentication


class AuthMiddleware(RemoteUserAuthentication):
    """We assume that authentication is already implemented in another service,
    and just check the provided header."""
    header = "HTTP_AUTHUSER"
