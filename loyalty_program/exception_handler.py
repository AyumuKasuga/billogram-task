from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    """We want to return error code along with the error message."""
    response = exception_handler(exc, context)
    response.data = exc.get_full_details()
    return response
