from django.contrib import admin
from django.urls import (
    path,
    include,
)
from django.http.response import HttpResponse
from discounts.api_v1 import urls as api_v1_urls


urlpatterns = [
    path("admin/", admin.site.urls),
    path("v1/", include((api_v1_urls, "discounts"))),
    path("healthcheck/", lambda r: HttpResponse('ok'))
]

urlpatterns = urlpatterns
