# Discounts Service

This microservice is a part of [Loyalty Program service.](DESIGN.md)

## API 

### Authentication

We assume that authentication is already implemented in another microservice, so here we fully rely on
`AUTHUSER` header which provides a username of an authenticated user.

### Authorization/Permissions

There are two roles in this service:

* `partner` - is an owner of a brand/market who can create campaigns.
* `customer` - is a customer who can receive discount codes.

Since this is a prototype -- we don't really check permissions and everyone can do everything.

### Endpoints

#### Partner endpoint

`/v1/campaign` - Only `POST` method (create) is implemented, if you want to change or delete your campaign -- please
contact our support team.

params:
`name` - Required. The human-readable name of campaign.
`max_codes` - Required. A number of discount codes you want to generate (Limited to 100 in the current implementation).

Example:
```
curl http://127.0.0.1:8000/v1/campaign \
    -H "Content-Type: application/json" \
    -H "AUTHUSER: partner" \
    -X POST \
    -d '{"name": "test", "max_codes": 20}'
```

Success response (201):
```json
{
    "name":"test",
    "id":"6f878dda-0309-498b-917f-dd8666ec75c4",
    "created_at":"2021-12-11T11:13:11.816139Z",
    "max_codes":20
}
```

Examples of failure responses (400):

```json
{
  "max_codes":[
    "Ensure this value is less than or equal to 100."
  ]
}
```

```json
{
  "max_codes":[
    "This field is required."
  ]
}
```

```json
{
  "name":[
    "This field is required."
  ]
}
```

#### Customer endpoint

`/v1/campaign/{campaign_id}/code` - Returns a discount code.

URL params:
`campaign_id` - ID of campaign.

Example:
```
curl http://127.0.0.1:8000/v1/campaign/6f878dda-0309-498b-917f-dd8666ec75c4/code \
-H "Content-Type: application/json" \
-H "AUTHUSER: customer"
```

Success response:
```json
{"code":"0610c194-e94f-436b-8b72-0cf2eae6f8b2"}
```

Examples of failure responses:

`404`

```json
{"message":"Not found.","code":"not_found"}
```


`400`

```json
[
  {
    "message":"This campaign is ended and we have no more codes.",
    "code":"no_more_codes"
  }
]
```

## How to run.

### Locally

You need something like this to run it locally.

```bash
pip install pipenv
pipenv install
python manage.py runserver
```

In the current version database credentials are hardcoded, sorry.

### Using docker-compose

```bash
docker-compose up
```

It will automatically run migrations if needed.

## Run tests

Config for CI is not implemented yet

First you need to run a postgres:
```bash
docker run --rm -e POSTGRES_USER=lp -e POSTGRES_PASSWORD=password -e POSTGRES_DB=lp --name db -p 5432:5432 postgres
```

Then you can run tests:
```bash
POSTGRES_HOST=localhost make test
```
