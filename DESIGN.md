# Loyalty Program Service

## Basic microservice structure and communication

```mermaid
flowchart TD
    Client[Web/mobile http client] --> LoadBalancer[Load balancer - ELB/Nginx/Envoy]
    LoadBalancer --> Auth
    Auth --> PartnerService
    Auth --> CustomerService
    subgraph Auth[Authentication Services]
        Auth1[Authentication Service] --> UserDB[(User DB)]
        Auth2[Authentication Service] --> UserDB[(User DB)]
    end
    subgraph PartnerService[Partner Services]
        Partner1[Partner Service]
        Partner2[Partner Service]
    end
    subgraph CustomerService[Customer Services]
        Customer1[Customer Service]
        Customer2[Customer Service]
    end
    PartnerService --> MarketplaceDB[(Brand/Stores DB)]
    CustomerService --> MarketplaceDB[(Brand/Stores DB)]
    ReplicaDB <--> MarketplaceDB
    CustomerService --> ReplicaDB[(RO Replica)]
    PartnerService --> SearchDB[(Elasticsearch)]
    CustomerService --> SearchDB[(Elasticsearch)]
    CustomerService --> |msg bus like Kafka|NotificationService
    subgraph NotificationService[Notification Services]
        Notification1[Notification Service]
        Notification2[Notification Service]
    end
    Prom(Prometheus) --> PartnerService
    Prom(Prometheus) --> CustomerService
    Prom(Prometheus) --> NotificationService
```

## Basic Description

Let's focus on Partner and customer service, we have two separate services to be able to scale them independently also it may be a different SLA for Partner and customer faced services. In this schema we use one shared database for brands/marketplaces/stores/discounts because they are all have relations to each other, customer service can only have Read-Only access to most of the tables (except discount codes of course), hence it can actually use a read-only replica to decrease a load to the main database.

In case if we will have a lot of stores and marketplaces -- we may need something like Elasticsearch to improve the search experience for our customers and decrease load of the main database.

Since partners want to be notified about users getting their discounts -- we need a notification service which can use different transports (like SMS, Email, Push, etc.). Customer service can use the notification service in async way using msg bus (SNS/Kafka/RabbitMQ).


## Authentication

For this implementation, I would use a separated auth service that manages registration/login/tfa and sets `AUTHUSER` header for downstream services. We assume that all the services are in one isolated/secure k8s cluster. Auth service can offer JWT token authentication for users to decrease load to User DB.

## Data storage

For the database I would use PostgreSQL just because I am familiar with it, also we have relations between models here and may need transactions/locks. Probably MongoDB or other document-oriented DB would also fit here. With PostgresQL we can use read only replicas for this case we are fine with possible lag between main and replica. Depending on the     amount of data we may need Elasticsearch as an addition. May also need some key-value storage for cache per service.

If we go with relational database like PostgreSQL we may have the following data model:

```mermaid
erDiagram
    BRAND ||--o{ STORE : contains
    BRAND {
        UUID id
        string name
    }
    STORE {
        UUID id
        UUID owner_user_id
        string name
    }
    STORE ||--o{ CAMPAIGN : contains
    CAMPAIGN {
        UUID id
        string name
        integer max_codes
    }
    CAMPAIGN ||--o{ CODE : contains
    CODE {
        UUID id
        string code
        UUID user_id
    }
```

## Async communication

Inside the services, we can use something like Celery to async task scheduling (nobody wants slow HTTP responses). Services are communicating mostly over sync HTTP API, the only exception I can see right now is a Notification Service.

## Scalability

We can use k8s HPA for autoscaling, number of requests per service can be a metric (we need to measure how many requests can handle one app instance (pod) first). For the Notification service metric for HPA can be number of messages in the queue or rate of consumed messages.

## Generating codes

We have two main approaches to generate code.

### Generate all the codes right away.

Pros:

* Overuse impossible, everything is already generated.
* We can assign codes for multiple users simultaneously without global lock.

Cons:

* The owner of a campaign should wait while codes are being generated.
* We store maximum number of codes anyway even they are not requested by users.

### Generate codes on-demand.

Pros:

* We can instantly create a campaign.
* Don't use extra space to store unassigned codes.

Cons:

* If we require to not overuse the amount of codes -- we need some kind of global lock, so only one customer can get a code, others should wait.
* If we don't use any global lock -- customers can get more codes than owner of the campaign was planned.
